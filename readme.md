# Mango exercieses:

You have to create the following component: <Range/>
You have to use React to create the solution.
You do NOT have to use any CLI to create structureand architecture of your application.
This component has two use modes:

* Normal range from min to max number
* Fixed number of options range

## Deploy
**https://mango-exercises-danielvinyas.surge.sh/**
