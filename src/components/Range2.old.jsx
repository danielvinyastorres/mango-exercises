import React from "react";

class Range2 extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            sliderWidth: 0,
            offsetSliderWidht: 0,
            min: 1,
            max: 100,
            minValueBetween: 1,
            
            currentMin: 1,
            inputMin: 1,
            
            currentMax: 100,
            inputMax: 100,
    
            rangeValue: [],
            currentMinRange: 0,
            currentMaxRange: 0,
        }

        this.setMin = this.setMin.bind(this);
        this.changeMinValue = this.changeMinValue.bind(this);
        this.onMouseMoveMin = this.onMouseMoveMin.bind(this);
        this.onMouseUpMin = this.onMouseUpMin.bind(this);

        this.setMax = this.setMax.bind(this);
        this.changeMaxValue = this.changeMaxValue.bind(this);
        this.onMouseMoveMax = this.onMouseMoveMax.bind(this);
        this.onMouseUpMax = this.onMouseUpMax.bind(this);
    }
  
    componentDidMount() {
        const URL = "https://demo2720578.mockable.io/mangoexercise2"
        
        fetch(URL, {
            "method": "GET", 
            "headers": 
                { 
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
        })
        .then(response => response.json())
        .then(response => {
            this.setState({
                rangeValue: response.rangeValue,

                min: response.rangeValue[0],
                max: response.rangeValue[response.rangeValue.length - 1],

                currentMin: response.rangeValue[0],
                inputMin:  response.rangeValue[0],
                
                currentMax: response.rangeValue[response.rangeValue.length - 1],
                inputMax: response.rangeValue[response.rangeValue.length - 1],
            })
        })
        .catch(err => {
            console.error(err);
        })
    
        const { currentMin, currentMax, max } = this.state;
        
        this.minValue.style.width = (currentMin*100)/max + "%";
        this.maxValue.style.width = (currentMax*100)/max + "%";
        
        this.setState({
            sliderWidth: this.slider.offsetWidth,
            offsetSliderWidht: this.slider.offsetLeft,
        })
    }
  
    setMin = (e) => {
        const { min, max, currentMax, minValueBetween } = this.state;
        const inputMin = e.target.value;
        
        this.setState({
            inputMin
        });
        
        if((inputMin >= min) && (inputMin <= (currentMax-minValueBetween))){
            this.setState({
                currentMin: parseInt(inputMin)
            }); 
    
            this.minValue.style.width = (inputMin*100)/max + "%";
        }
    }
  
    changeMinValue = (e) => {
        e.preventDefault();
    
        document.addEventListener('mousemove', this.onMouseMoveMin);
        document.addEventListener('mouseup', this.onMouseUpMin);
        
        document.addEventListener('touchmove', this.onMouseMoveMin);
        document.addEventListener('touchend', this.onMouseUpMin);
    }
  
    onMouseMoveMin = (e) => {
        const { min, max, currentMax, minValueBetween, sliderWidth, offsetSliderWidht, rangeValue } = this.state;
        
        const dragedWidht = e.clientX - offsetSliderWidht;
        const dragedWidhtInPercent = parseInt((dragedWidht*100)/sliderWidth);
        const currentMin = parseInt((max * dragedWidhtInPercent)/100);
   
        if( (currentMin >= min) && (currentMin <= (currentMax-minValueBetween))){
            let rangeCurrentMin = rangeValue.reduce(function(prev, curr) {
                return (Math.abs(curr - currentMin) < Math.abs(prev - currentMin) ? curr : prev);
            });

            this.minValue.style.width = dragedWidhtInPercent + "%";
            this.minValue.dataset.content = `${rangeCurrentMin} €`;

            this.setState({
                currentMin: rangeCurrentMin,
                inputMin: rangeCurrentMin,

                currentMinRange: rangeCurrentMin,
            })
        }
    }
  
    onMouseUpMin = () => {
        const { currentMinRange, max } = this.state;
        
        document.removeEventListener('mouseup', this.onMouseUpMin);
        document.removeEventListener('mousemove', this.onMouseMoveMin);
        
        document.removeEventListener('touchend', this.onMouseMoveMin);
        document.removeEventListener('touchmove', this.onMouseUpMin);

        this.minValue.style.width = parseInt(((currentMinRange / max) * 100) - 2) + "%";
    }
    
    setMax = (e) => {
        const { max, currentMin, minValueBetween } = this.state;
    
        const inputMax = e.target.value;
        
        this.setState({
                inputMax
        });
  
        if((inputMax >= currentMin + minValueBetween) && (inputMax <= max)){
            this.setState({
                currentMax: parseInt(inputMax)
            });
            this.maxValue.style.width = (inputMax*100)/max + "%";
        } 
    }
    
    changeMaxValue = (e) => {
        e.preventDefault();
    
        document.addEventListener('mousemove', this.onMouseMoveMax);
        document.addEventListener('mouseup', this.onMouseUpMax);
        
        document.addEventListener('touchmove', this.onMouseMoveMax);
        document.addEventListener('touchend', this.onMouseUpMax);
    }
  
    onMouseMoveMax = (e) => {
        const { max, currentMin, minValueBetween, sliderWidth, offsetSliderWidht, rangeValue} = this.state; 

        const dragedWidht = e.clientX - offsetSliderWidht;
        const dragedWidhtInPercent = parseInt((dragedWidht*100)/sliderWidth);
        const currentMax = parseInt((max * dragedWidhtInPercent)/100);
      
        if( (currentMax >= (currentMin + minValueBetween)) && (currentMax <= max)){
            let rangeCurrentMax = rangeValue.reduce(function(prev, curr) {
                return (Math.abs(curr - currentMax) < Math.abs(prev - currentMax) ? curr : prev);
            });

            this.maxValue.style.width = dragedWidhtInPercent + "%";
            this.maxValue.dataset.content = `${rangeCurrentMax} €`;
                
            this.setState({
                currentMax: rangeCurrentMax,
                inputMax: rangeCurrentMax,

                currentMaxRange: rangeCurrentMax,
            })
        }
    }
  
    onMouseUpMax = () => {
        const { currentMaxRange, max } = this.state;
        
        document.removeEventListener('mouseup', this.onMouseUp);
        document.removeEventListener('mousemove', this.onMouseMoveMax);
        
        document.removeEventListener('touchend', this.onMouseUp);
        document.removeEventListener('touchmove', this.onMouseMoveMax);

        this.maxValue.style.width = parseInt(((currentMaxRange / max) * 100) + 2) + "%";
    }
    
    render() {
        const { min, max, currentMin, inputMin, currentMax, inputMax } = this.state;
      
        return (
            <div className="card"> 
            <h2>Fixed Value Range</h2>

                <div className="values">
                    <div className="currencyLabel">Min: { inputMin } €</div>
                    <div className="currencyLabel">Max: { inputMax } €</div>
                </div>


                <div className="all-inputs">
                    <div className="minMaxNumber">{ min } €</div>   

                    <div ref={ref => this.slider = ref} id="slider">

                        <div ref={ref => this.minValue = ref} id="min" data-content={currentMin + " €"}>
                            <div ref={ref => this.minValueDrag = ref} id="min-drag" onMouseDown ={this.changeMinValue} onTouchStart={this.changeMinValue}></div>     
                        </div>

                        <div ref={ref => this.maxValue = ref} id="max" data-content={currentMax + " €"}>
                            <div ref={ref => this.maxValueDrag = ref} id="max-drag" onMouseDown={this.changeMaxValue} onTouchStart={this.changeMaxValue}></div>
                        </div>

                    </div>

                    <div className="minMaxNumber">{ max } €</div>
                </div>
            </div>
        )
    }
  }

export default Range2;
