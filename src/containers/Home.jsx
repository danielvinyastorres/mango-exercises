import { NavLink } from "react-router-dom"
import React from 'react';

const Home = () => {

    return (
        <div className="range-content">
            <NavLink
            to = {{pathname: "exercise1/"}}
            className="nav-button">
                Normal Range
            </NavLink>
            <NavLink
            to = {{pathname: "exercise2/"}}
            className="nav-button">
                Fixed values range
            </NavLink>
        </div>
    )
}

export default Home