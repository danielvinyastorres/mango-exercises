import React from 'react';
import Range1 from '../components/Range1';
import { NavLink } from "react-router-dom"


const Exercise1 = () => {

    return (
        <div className="range-content">            
            <Range1 />
            
            <NavLink
                to = {{pathname: "/exercise2/"}}
                className="nav-button">
                    Fixed values range
            </NavLink> 
        </div>
    )
}

export default Exercise1