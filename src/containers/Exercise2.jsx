import React from 'react';
import { NavLink } from "react-router-dom"
import Range2 from '../components/Range2';

const Exercise2 = () => {

    return (
        <div className="range-content">
            <Range2 />
            <NavLink
                to = {{pathname: "/exercise1/"}}
                className="nav-button">
                    Normal Range
            </NavLink> 

        </div>
    )
}

export default Exercise2