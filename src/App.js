import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import React from "react";
import Home from "./containers/Home"
import Exercise1 from "./containers/Exercise1";
import Exercise2 from "./containers/Exercise2";

const App = () => {
    return (
        <div>
            <h1><span className="mango-title">MANGO</span> Range Exercises</h1>
            <Router>
                <div>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route exact path='/exercise1' component={Exercise1} />
                        <Route exact path='/exercise2' component={Exercise2} />
                    </Switch>
                </div>
            </Router>
        </div>
    );
  }

export default App;
